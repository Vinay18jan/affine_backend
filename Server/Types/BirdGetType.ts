export class BirdGetType {
    Id:number = undefined;
    BirdName:string = undefined;
    Continents:string = undefined;
    BirdFamily:string = undefined;
    AddedOn:string = undefined;
    Visible: boolean = undefined;
}