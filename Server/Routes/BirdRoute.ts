import express from 'express';
import * as BirdController from '../Controller/BirdController';
import { ValidateRequest, ValidateBirdCreate } from '../Utility/Validate';
let birdrouter = express.Router();

birdrouter.post('/bird', ValidateRequest,ValidateBirdCreate, BirdController.AddBird);
birdrouter.get('/birds', BirdController.GetAllBirds);
birdrouter.get('/bird/:id', BirdController.GetBirdById);
birdrouter.delete('/bird/:id', BirdController.DeleteBird);
birdrouter.get('/continents', BirdController.GetContinents);

export default birdrouter;