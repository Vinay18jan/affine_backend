import SqlHelper from "../Utility/MySqlhelper";
import { BirdGetType } from "../Types/BirdGetType";
import { isNullOrUndefined } from "util";

export const AddBird = (req, res, done: Function) => {
    try {
        let sqlHelper = new SqlHelper();
        var params = {
            'p_name': req.body.birdname,
            'p_family_name': req.body.familyname,
            'p_continent_ids': req.body.continentids,
            'p_is_visible': (req.body.isvisible != undefined) ? JSON.parse(req.body.isvisible) : false
        }
        sqlHelper.ExecuteSP('add_bird', params, function (error, result) {
            if (error) {
                done(error, undefined);
            }
            else if (result.length > 0) {
                done(undefined, result[0][0]);
            }
        })
    }
    catch (ex) {
        done(ex, undefined)
    }
}

export let GetAllBirds = (req, res, done: Function) => {
    let sqlHelper = new SqlHelper();
    sqlHelper.ExecuteSP('get_all_birds', {}, function (error, results) {

        let searchResult = new Array();

        if (error) {
            done(error, undefined);
        }
        else if (results.length > 0) {

            if (results[0] != undefined) {
                results[0].forEach(x => {
                        searchResult.push(x);
                });
            }

            done(undefined, searchResult);
        }
    });
}

export let GetBirdById = (req, res, done: Function) => {
    let sqlHelper = new SqlHelper();
    var params = {
        'p_bird_id': req.params.id
    };
    sqlHelper.ExecuteSP('get_bird_by_id', params, function (error, results) {

        let searchResult: BirdGetType = new BirdGetType();
        if (error) {
            done(error, undefined);
        }
        else if (!isNullOrUndefined(results) && !isNullOrUndefined(results[0]) && results[0].length > 0) {
            if (results[0] != undefined) {
                searchResult.Id = results[0][0].id;
                searchResult.BirdName = results[0][0].bird_name;
                searchResult.Continents = results[0][0].continents;
                searchResult.BirdFamily = results[0][0].bird_family;
                searchResult.AddedOn = results[0][0].added_on;
                searchResult.Visible = results[0][0].visible;
            }

            done(undefined, searchResult);
        }
        else done(undefined, undefined);
    });
}

export let DeleteBird = (req, res, done: Function) => {
    try {
        let sqlHelper = new SqlHelper();
        var params = {
            'p_bird_id': req.params.id
        }
        sqlHelper.ExecuteSP('delete_bird', params, function (error, result) {
            if (error) {
                done(error, undefined);
            }
            else if (result.length > 0) {
                done(undefined, result[0][0]);
            }
        })
    }
    catch (ex) {
        done(ex, undefined)
    }
}

export let GetContinents = (req, res, done: Function) => {
    let sqlHelper = new SqlHelper();
    sqlHelper.ExecuteSP('get_continents', {}, function (error, results) {

        let searchResult = new Array();

        if (error) {
            done(error, undefined);
        }
        else if (results.length > 0) {

            if (results[0] != undefined) {
                results[0].forEach(x => {
                        searchResult.push(x);
                });
            }

            done(undefined, searchResult);
        }
    });
}