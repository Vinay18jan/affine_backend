require('dotenv-flow').config();
import express from 'express';
import birdrouter from './Routes/BirdRoute'
import bodyparser from 'body-parser';
const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

app.use(function (req: any, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin,x-xsrf-token,X-Requested-With,Content-Type, Accept, Z-Key");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    if (req.method == 'OPTIONS') {
      res.sendStatus(200);
    }
    else {
      next();
    }
  });
app.use(birdrouter);


  
export default app;