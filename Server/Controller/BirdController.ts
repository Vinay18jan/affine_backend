import { Request, Response, NextFunction } from "express";
import * as BirdModel from '../Model/BirdModel'

export let AddBird = (req: Request, res: Response, next: NextFunction) => {
    try {
        BirdModel.AddBird(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": '',
                    "message": "Created Successfully!!!!"
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to create!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

export let GetAllBirds = (req: Request, res: Response, next: NextFunction) => {
    try {
        BirdModel.GetAllBirds(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": result,
                    "message": ""
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to fetch!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

export let GetBirdById = (req: Request, res: Response, next: NextFunction) => {
    try {
        BirdModel.GetBirdById(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": result,
                    "message": ""
                });
                next();
            }
            else {
                res.status(404).json({
                    "status": false,
                    "data": "",
                    "message": 'NOT FOUND!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

export let DeleteBird = (req: Request, res: Response, next: NextFunction) => {
    try {
        BirdModel.DeleteBird(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": '',
                    "message": "Deleted Successfully!!!"
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to fetch!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}

export let GetContinents = (req: Request, res: Response, next: NextFunction) => {
    try {
        BirdModel.GetContinents(req,res,function(err, result){
            if(err){
                res.status(501).json({
                    "status": false,
                    "data": '',
                    "message": err.message
                });
                next(err);
            }
            else if (result != undefined) {
                res.status(200).json({
                    "status": true,
                    "data": result,
                    "message": ""
                });
                next();
            }
            else {
                res.status(200).json({
                    "status": false,
                    "data": "",
                    "message": 'Failed to fetch!!!'
                });
                next();
            }
        })
    }
    catch(ex){
        next(ex);
    }
}