import { Request, Response, NextFunction } from "express";
import { isNullOrUndefined } from "util";

const badStrings = ["'", '"', '<', '>'];


export let ValidateRequest = (req: Request, res: Response, next: NextFunction) => {
    if (HasBadInput(req.body)) {
        res.status(400).send(
            {
                "status": false,
                "data": "",
                "message": 'BAD REQUEST!!!!'
            })
    }
    else
        next();
}

function HasBadInput(requestParams) {
    for (var key in requestParams) {
        var value = requestParams[key];

        if (value && IsBadInput(value)) {
            return true;
        }
    }
    return false;
}

function IsBadInput(value) {
    for (var i = 0; i < badStrings.length; i++) {
        value += ""; // convert to string

        if (value.indexOf(badStrings[i]) > -1) {
            return true;
        }
    }
    return false;
}

export let ValidateBirdCreate = (req: Request, res: Response, next: NextFunction) => {
    if(isNullOrUndefined(req.body.birdname) || isNullOrUndefined(req.body.familyname) 
    || isNullOrUndefined(req.body.continentids)) {
        res.status(400).send(
            {
                "status": false,
                "data": "",
                "message": 'BAD REQUEST!!!!'
            })
    }
    else
        next();
}